import React from 'react';
import {NavLink} from 'react-router-dom'
import{Meteor} from 'meteor/meteor'
import { connect } from 'react-redux';

export  class Login extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
        error: ''
      };
 
    }
    componentWillUnmount(){
      Meteor.subscribe('myuser');
      console.log(Meteor.user())
    }

    onSubmit(e) {
      e.preventDefault();
      let email = this.refs.email.value.trim();
      let password = this.refs.password.value.trim();
      Meteor.loginWithPassword({email}, password, (err) => {
   if (err) {
          this.setState({error: 'Unable to login. Check email and password.'});
        } else {
        
          console.log("user from Login", Meteor.user().dates)
          this.setState({error: ''});
          if(Meteor.user().isAdmin==true)
          {
         
            this.props.history.replace('/admin')
            alert('Admin')
          }
          else{
            this.props.history.replace('/Reserve')

          }
        }
      });
    }
    render() {
      return(
      <div>
                   {this.state.error ? <p>{this.state.error}</p> : undefined}

           <form onSubmit={this.onSubmit.bind(this)}>
            <input type="email" ref="email" name="email" placeholder="Email"/>
            <input type="password" ref="password" name="password" placeholder="Password"/>
            <button>Login</button>
          </form>
  
        <NavLink to="/signup">Create  an account</NavLink>
      <p>Login component here</p>;
      
      </div>
      )
    }
  }
  const mapStateToProps = (state) => {
    return {
  
    ...state
  
    }
  }
  const mapDispatchersToProps = (dispatcher) => {
    return {
      setTasksDataToStore: (TasksDataArray) => dispatcher({ type: 'SET_TASKSDATA', value: TasksDataArray })
    }
  }
  
connect(mapStateToProps, mapDispatchersToProps)(Login);
  