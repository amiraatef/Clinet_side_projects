var webpack = require('webpack');
var path = require('path');
// const BundleInspector = require('bundle-inspector-webpack-plugin');
// var BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
var CompressionPlugin = require("compression-webpack-plugin");
const autoprefixer = require('autoprefixer');
const postcssUrl = require('postcss-url');
const cssnano = require('cssnano');
const customProperties = require('postcss-custom-properties');
const minimizeCss = false;
const baseHref = "";
var fs = require('fs');
const lessToJs = require("less-vars-to-js");
const themeVariables = lessToJs(
    fs.readFileSync(path.join(__dirname, "./theme.less"), "utf8")
);
const deployUrl = "";

const postcssPlugins = function () {
    // safe settings based on: https://github.com/ben-eb/cssnano/issues/358#issuecomment-283696193
    const importantCommentRe = /@preserve|@licen[cs]e|[@#]\s*source(?:Mapping)?URL|^!/i;
    const minimizeOptions = {
        autoprefixer: false,
        safe: true,
        mergeLonghand: false,
        discardComments: { remove: (comment) => !importantCommentRe.test(comment) }
    };
    return [
        postcssUrl([
            {
                // Only convert root relative URLs, which CSS-Loader won't process into require().
                filter: ({ url }) => url.startsWith('/') && !url.startsWith('//'),
                url: ({ url }) => {
                    if (deployUrl.match(/:\/\//)) {
                        // If deployUrl contains a scheme, ignore baseHref use deployUrl as is.
                        return `${deployUrl.replace(/\/$/, '')}${url}`;
                    }
                    else if (baseHref.match(/:\/\//)) {
                        // If baseHref contains a scheme, include it as is.
                        return baseHref.replace(/\/$/, '') +
                            `/${deployUrl}/${url}`.replace(/\/\/+/g, '/');
                    }
                    else {
                        // Join together base-href, deploy-url and the original URL.
                        // Also dedupe multiple slashes into single ones.
                        return `/${baseHref}/${deployUrl}/${url}`.replace(/\/\/+/g, '/');
                    }
                }
            },
            {
                // TODO: inline .cur if not supporting IE (use browserslist to check)
                filter: (asset) => !asset.hash && !asset.absolutePath.endsWith('.cur'),
                url: 'inline',
                // NOTE: maxSize is in KB
                maxSize: 10
            }
        ]),
        autoprefixer({grid: true}),
        customProperties({ preserve: true })
    ].concat(minimizeCss ? [cssnano(minimizeOptions)] : []);
};

module.exports = {
    entry: {
      app_vendors: ["react", "react-dom"],
      common: './src/index.js'
    },
    devtool: "eval-source-map",
    watch: true,
    output: {
        filename: './public/js/[name].min.js',
        chunkFilename: './public/js/[id].[hash].chunk.js',
        publicPath: '/'
    },
    node: {
      fs: 'empty'
    },
    "externals": [
        resolveExternals
    ],
    module: {
        loaders: [{
            test: /antd.*\.less$/,
            use: [
                
                { loader: "style-loader" },
                { loader: "css-loader"},
                {
                    loader: "less-loader",
                    options: {
                        javascriptEnabled: true,
                        modifyVars: themeVariables
                    }
                }
            ]
        },
   
        {
            test: /\.less$/,
            exclude: /(node_modules|bower_components)/,
            use: [
                { loader: "style-loader" },
                { loader: "css-loader", options: {
                    module: true
                }},
                {
                    loader: "less-loader",
                    options: {
                        javascriptEnabled: true,
                        modifyVars: themeVariables
                    }
                }
            ]
        }, {
          test: /\.css$/,
          use: [
                { loader: "style-loader" },
                { loader: "css-loader"}
            ]
        }, {
          test: /\.scss$/,
          use: [{
              loader: "style-loader" // creates style nodes from JS strings
          }, {
              loader: "css-loader" // translates CSS into CommonJS
          }, {
            loader: "postcss-loader",
            "options": {
               "ident": "postcss",
               "plugins": postcssPlugins,
               "sourceMap": false
             }

          }, {
              loader: "sass-loader" // compiles Sass to CSS
          }],
          exclude: /node_modules/,
        },
        {
          "test": /\.(jpg|png|webp|gif|otf|ttf|woff|woff2|ani|eot)$/,
          "loader": "url-loader",
          "options": {
            "name": "[name].[hash:20].[ext]",
            "limit": 100000
          }
        },
        {
            test: /\.svg$/,
            loader: 'svg-inline-loader'
        },
         {
           test: /\.es6$/,
           exclude: /node_modules/,
           loader: 'babel-loader'
         }, {
           test: /\.jsx/,
           loader: 'babel-loader',
           exclude: /(node_modules|bower_components)/
         }, {
          test: /\.js$/,
          exclude: /(node_modules|bower_components)/,
          loader: 'babel-loader'
        }, {
            test: /\.html$/,
            loader: "html-loader"
        }]
    },
    resolve: {
      modules: [
        path.resolve('./import'),
        path.resolve('./node_modules')
      ],
       extensions: ['.js', '.es6', '.jsx']
     },
     plugins: [
        //  "transform-runtime",
        //  ["transform-es2015-modules-commonjs-simple", {
        //      "noMangle": true
        //  }],
        // new webpack.IgnorePlugin(/^\.\/locale$/, [/moment$/]),
        // new webpack.NoErrorsPlugin(),
      //   new CompressionPlugin({
      //     asset: "[path].gz[query]",
      //     algorithm: "gzip",
      //     test: /\.js$|\.css$|\.html$/,
      //     threshold: 10240,
      //     minRatio: 0
      //   }),
      //   new webpack.optimize.CommonsChunkPlugin({
        //      // The order of this array matters
        //      names: ["common", "vendor"],
        //      minChunks: Infinity
        //  }),
      //   new webpack.DefinePlugin({ // <-- key to reducing React's size
      //     'process.env': {
      //       'NODE_ENV': JSON.stringify('production')
      //     }
      //   }),
        // new webpack.optimize.DedupePlugin(), //dedupe similar code
        // new webpack.optimize.UglifyJsPlugin(), //minify everything
      //   new webpack.optimize.AggressiveMergingPlugin(),//Merge chunks
      //     new BundleAnalyzerPlugin(),
      //
      //     // new webpack.NoErrorsPlugin(),
      ]
}

function resolveExternals(context, request, callback) {
    return resolveMeteor(request, callback) ||
        callback();
}

function resolveMeteor(request, callback) {
    var match = request.match(/^meteor\/(.+)$/);
    var pack = match && match[1];
    if (pack) {
        console.log(pack);
        callback(null, 'Package["' + pack + '"]');
        return true;
    }
}
