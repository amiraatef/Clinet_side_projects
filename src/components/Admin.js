import React, { Component } from 'react'
import { Meteor } from 'meteor/meteor'
import moment from 'moment'

export class AdminPage extends Component {

  onlogout = () => {
    // Meteor.subscribe('myuser').stop()

    // Accounts.logout()
    Meteor.logout()
    
    this.props.history.push("/")
  }

  Cancelled=(date,e)=>
  {
   console.log( Meteor.users)
Meteor.call("cancelledDate",date)
  }

  approved=(date,e)=>{
console.log("open")
  Meteor.call("approvedDate",date)
  }
  render() {
    Meteor.subscribe('myuser'); 
    console.log("AdminUser", Meteor.user().dates)
    return (
      <div>
        <button onClick={this.onlogout.bind(this)}> Logout</button>
        <table>
          <tr>
            <th>Reservation Time</th>
            <th >approved</th>
            <th >Cancelled</th>
          </tr>
          {Meteor.user().dates.map((date) => {
            console.log(moment(date.oldTo).format('hh:mm:ss A').toString())
            return (

              <tr>
                <td >
                  <p>  To {date.customerName}</p>

                  <p>From: {moment(date.OldFrom).format('hh:mm:ss A').toString()}</p>
                  <p>To: {moment(date.oldTo).format('hh:mm:ss A').toString()}</p>
                  <p>Day: {moment(date.SelectedDate).format('MMMM/dddd/YYYY')}</p>
                  <p> Customer Name: {date.customerName}</p>


                </td>
                <td><button onClick={this.Cancelled.bind(this,date)}>Cancelled</button></td>
                <td><button onClick={this.approved.bind(this,date)}>approved</button></td>

              </tr>

            )


          })}

        </table>
      </div>
    )
  }
}
