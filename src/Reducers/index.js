import {combineReducers} from 'redux'
import nuggetReducer from './reducer';
export default combineReducers({
    nuggets:nuggetReducer,
})
