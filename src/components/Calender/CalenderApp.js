import React, { Component } from 'react';
import './Style.css';
import moment from 'moment'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton'
import {Tasks }from './Tasks';
import {TimeSchedule} from './Timesolts'
import{ Days} from "./Days";
import { connect } from 'react-redux';
import { Checkbox } from 'material-ui';
import FaCalendar from 'react-icons/lib/fa/calendar'
import {DialogForm} from './dailogForm'
import { getFormValues } from 'redux-form'
import shortid from 'shortid'
import { Meteor } from 'meteor/meteor';

export class App extends Component {
  constructor(props) {
    super(props);
    this.handleClose = this.handleClose.bind(this);
    //  const Dates= Meteor.user().dates
  }

  handleOpen = () => {
    this.props.setOpenFlag(true);
  };
  handleAlertPopUpClose = () => {
    this.props.setFormValidity(false);
  };
  handleCloseCancell = () => {
    this.props.setOpenFlag(false);
  }
  onlogout = () => {
    debugger;
   if( Meteor.user.dates.length>0)
   {
    Meteor.user.dates.map((date) => {
    //   debugger;
    if(date.Status === 'pendding')
    {
      console.log('date will be sent ' , {
        userID: Meteor.userId(),
        ...date
      })
      Meteor.call('addToAdmin',{
        userID: Meteor.userId(),
        ...date
      })
    }
 
    })
  }

    Meteor.logout()

    this.props.history.push("/")

  }
  handleClose = () => {
    this.props.setOpenFlag(false);
    let TimesTobeAdded = []
    let Zone = this.props.Dailog.values.ZoneNumber
    let CustomerName = this.props.Dailog.values.CustomerName

    let From = this.props.Dailog.values.FromTime;
    let To = this.props.Dailog.values.ToTime;
    var start = moment(From, ' hh:mm a');
    var end = moment(To, 'hh:mm a');
    var begging = moment('09:00 am', 'hh:mm a')
    var final = moment('06:00 pm', 'hh:mm a')

    for (var i = 0; i < this.props.timeslots.length; i++) {
      var temp = moment(this.props.timeslots[i], 'hh:mm a')
      if (temp.isBetween(start, end)) {
        TimesTobeAdded.push(i + 1)
      }
    }
    TimesTobeAdded.unshift(TimesTobeAdded[0] - 1);
    TimesTobeAdded.push(TimesTobeAdded[TimesTobeAdded.length - 1] - 1);




    var cell1 = document.getElementById("Table").rows[TimesTobeAdded[0]].cells.item(Zone);
    var bod = document.querySelector('body').getBoundingClientRect();
    var first = cell1.getBoundingClientRect();

    debugger;
    let _TasksData;
    if (Meteor.user().dates.length > 0) {
      _TasksData = Meteor.user().dates
    }
    else {
      _TasksData = []
    }
     /*[...this.props.TasksData]*/;

    let newTask = {
      top: -1 * (bod.top - first.top),
      left: first.left,
      height: first.height * TimesTobeAdded.length,
      width: first.width,
      customerName: CustomerName,
      zone: Zone,
      OldFrom: From /*start*/,
      oldTo: To /*end*/,
      SelectedDate: moment(this.props.OldDate).format(),
      ID: shortid.generate(),
      Status: "pendding"
    }
    debugger;
    _TasksData.push(newTask);
    this.props.setTasksDataToStore(_TasksData);
    Meteor.call('addReservation', newTask)


  };
  
  render() {
    debugger;
    let _Tasks = null;
    let temp = []
    let sendingprops = []
   
    console.log("user from Render Calender ", Meteor.user().dates)
    console.log("Calender Render tasksdata",this.props.TasksData)


    if ( Meteor.user().dates.length> 0) {
      debugger;
      for (let key in Meteor.user().dates) {
        if (moment(Meteor.user().dates[key].SelectedDate).format('MMMM/dddd/YYYY') == moment(this.props.OldDate).format('MMMM/dddd/YYYY')) {
          temp.push( Meteor.user().dates[key])
          _Tasks = <Tasks TasksData={temp} />

        }

      }

    }

    return (
      <MuiThemeProvider>

        <div className="AppAppWrapper" >
          <button onClick={this.onlogout.bind(this)}> Logout</button>
          <div className="Appitem1">
            <Days />
            <br />
            <RaisedButton style={{ marginRight: "83%" }} label="Reserve a zone" primary={true} onClick={this.handleOpen} />

          </div>

          <div className="Appitem2">
            <div>
              <br />
              <label > Filter view …</label>
            </div>
            <br />
            <br />

            <p >Select by Service Providor(s)</p>

            <div >  <Checkbox label="User1" /></div>
            <div >  <Checkbox label="User2" /></div>
            <div >  <Checkbox label="User3" /></div>
            <div >  <Checkbox label="User4" /></div>
            <RaisedButton label="ALL" primary={true} style={{ width: "50%" }} />
            <RaisedButton label="Clear" secondary={true} style={{ width: "50%" }} />
            <br />
            <br />
            <RaisedButton label="In Today" primary={true} style={{ width: "50%" }} />
            <RaisedButton label="In On Date" secondary={true} style={{ width: "50%" }} /> <FaCalendar />
            <br />
            <p className="Appnotification">Shows calendars of service <br />
              providors who provide this <br />
              service</p>
            <p> Select by Zone (Space)</p>

            <div >  <Checkbox label="Zone1" /></div>
            <div >  <Checkbox label="Zone2" /></div>
            <div >  <Checkbox label="Zone3" /></div>
            <div >  <Checkbox label="Zone4" /></div>
            <RaisedButton label="ALL" primary={true} style={{ width: "50%" }} />
            <RaisedButton label="Clear" secondary={true} style={{ width: "50%" }} />
            <br />
            <br />
            <RaisedButton label="In Today" primary={true} style={{ width: "50%" }} />
            <RaisedButton label="In On Date" secondary={true} style={{ width: "50%" }} /> <FaCalendar />
            <br />
            <br />
            <div style={{ border: "2px solid", textAlign: "center" }}>

              <span> Has Availabilty On Date <FaCalendar /> </span>

            </div>
            <p className="Appnotification">By Zone: For restaurants,<br />
              multi-theatre cinemas, and <br />
              any “space” bookings
                 </p>
          </div>


          <div className="Appitem3">
            <TimeSchedule />
          </div>

        </div>
        <div>
          {_Tasks}
          <Dialog
            title="Please insert From: to: zoneNumber"
            modal={true}
            open={this.props.open}
          >

           {/* currentDate={ this.props.OldDate} */}

            <DialogForm cancelled={this.handleCloseCancell} currentDate={ this.props.OldDate} times={temp} onSubmit={this.handleClose} />
          </Dialog >

        </div>
      </MuiThemeProvider>
    );
  }


}
const mapStateToProps = (state) => {
  return {

    ...state.app,
    ...state.form

  }
}

const mapDispatchersToProps = (dispatcher) => {
  return {
    setTasksDataToStore: (TasksDataArray) => dispatcher({ type: 'SET_TASKSDATA', value: TasksDataArray }),
    setFormValidity: (formValidity) => dispatcher({ type: 'SET_FORM_VALIDITY', value: formValidity }),
    setOpenFlag: (openFlag) => dispatcher({ type: 'SET_OPEN_FLAG', value: openFlag }),
    setFromField: (FromFieldValue) => dispatcher({ type: 'SET_FORM_FIELD', value: FromFieldValue }),
    setToField: (ToFieldValue) => dispatcher({ type: 'SET_TO_FIELD', value: ToFieldValue }),
    setZoneNumberField: (zoneNumberFieldValue) => dispatcher({ type: 'SET_ZONENUMBER_FIELD', value: zoneNumberFieldValue }),
    setCustomerNameField: (CustomerNameFieldValue) => dispatcher({ type: 'SET_CUSTOMER_NAME_FIELD', value: CustomerNameFieldValue }),
    SETMessage: (MessageFieldValue) => dispatcher({ type: 'SET_message', value: MessageFieldValue }),
  }
}

 connect(mapStateToProps, mapDispatchersToProps)(App);
