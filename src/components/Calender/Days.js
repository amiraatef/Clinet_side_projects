import React, { Component } from 'react'
import './Style.css';
import RaisedButton from 'material-ui/RaisedButton'
import {Calender} from './calenderComponent'
import moment from 'moment'
import { connect } from 'react-redux';

 export class Days extends Component {
     handleChange=(value,e)=>
    {
       
        if(value==1)
        {
          
            this.props.SETDate( moment().add(+1, 'days').format())
        }
        if(value==2)
        {
          
            this.props.SETDate( moment().add(+2, 'days').format())

        }
        if(value==3)
        {
          
            this.props.SETDate( moment().add(+3, 'days').format())

        }
        if(value==4)
        {
          
            this.props.SETDate( moment().add(+4, 'days').format())

        }
        if(value==5)
        {
          
            this.props.SETDate( moment().add(+5, 'days').format())

        }
        if(value==6)
        {
          
            this.props.SETDate( moment().add(+6, 'days').format())

        }
        if(value==7)
        {
          
            this.props.SETDate( moment().add(+7, 'days').format())

        }
        if(value==0)
        {
          
            this.props.SETDate( moment().format())

        }
        if(value==-1)
        {
          
            this.props.SETDate( moment().add(-1, 'days').format())

        }
        if(value==-2)
        {
            this.props.SETDate( moment().add(-2, 'days').format())
        }
        if(value==-3)
        {
            this.props.SETDate( moment().add(-3, 'days').format())
        }
        if(value==-4)
        {
            this.props.SETDate( moment().add(-4, 'days').format())
        }
        if(value==-5)
        {
            this.props.SETDate( moment().add(-5, 'days').format())
        }
        if(value==-6)
        {
            this.props.SETDate( moment().add(-6, 'days').format())
        }
        if(value==-7)
        {
            this.props.SETDate( moment().add(-7, 'days').format())
        }
        if(value==-8)
        {
            this.props.SETDate( moment().add(-8, 'days').format())
        }
    }
    render() {
        let today  = moment( this.props.app.OldDate).format('MMMM/dddd/YYYY')

        return (
            <div>
                <br />
                <div className="Dayswrapper" >
                    <div className="DaysCalender">
                        <Calender />

                    </div>
                    <div className="Daysbtns">
                        <button >Day</button>
                        <button>Week</button>
                        <button>Month</button>
                        <button>Year</button>
                    </div>
                    <div className="DaysnestedGrid">
                        <button className="Daysprevious"> &#8249;</button>
                        <div>{today}</div>
                        <button className="Daysnext" > &#8250; </button>
                    </div>
                    <div className="Daysitem">
                       <label>Weeks</label>
                        <button onClick={this.handleChange.bind(this,+1 )} >  +1</button>
                        <button  onClick={this.handleChange.bind(this ,+2)}>  +2</button>
                        <button  onClick={this.handleChange.bind(this ,+3)} >  +3</button>
                        <button  onClick={this.handleChange.bind(this,+4)} >  +4</button>
                        <button  onClick={this.handleChange.bind(this ,+5)} >  +5</button>
                        <button  onClick={this.handleChange.bind(this ,+6)}>  +6</button>
                        <button  onClick={this.handleChange.bind(this ,+7 )}>  +7</button>
                        <button  onClick={this.handleChange.bind(this,+8 )}>  +8</button>
                        <button  onClick={this.handleChange.bind(this ,0) }> today</button>
                        <button  onClick={this.handleChange.bind(this ,-1)}>  -1</button>
                        <button  onClick={this.handleChange.bind(this ,-2)} >  -2</button>
                        <button  onClick={this.handleChange.bind(this ,-3)} >  -3</button>
                        <button  onClick={this.handleChange.bind(this ,-4)}>  -4</button>
                        <button  onClick={this.handleChange.bind(this ,-5)} >  -5</button>
                        <button  onClick={this.handleChange.bind(this ,-6)}>  -6</button>
                        <button  onClick={this.handleChange.bind(this,-7)}>  -7</button>
                        <button  onClick={this.handleChange.bind(this,-8)}> -8</button>
                        <label>Weeks</label>
                    </div>
                </div >

            </div >
        )
    }
}
const mapStateToProps = (state) => {
    return {
  
      ...state
  
    }
  }
  const mapDispatchersToProps = (dispatcher) => {
    return {
        SETDate : (newDate)=>dispatcher({type:'SetDate_', value: newDate})
    }
  }

 connect(mapStateToProps, mapDispatchersToProps)(Days);

