import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'meteor-client'
import {AppRoutes} from './Routes/AppRoutes'
import {Meteor} from 'meteor/meteor'

// import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<AppRoutes />, document.getElementById('root'));
// registerServiceWorker();
