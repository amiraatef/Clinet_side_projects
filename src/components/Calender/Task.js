import React, { Component } from 'react'
import './Style.css';

import FaInfoCircle from 'react-icons/lib/fa/info-circle'

export  class Task extends Component {
  render() {

    let style={
        position:'absolute',
        left: this.props.left,
        top: this.props.top,
        height: this.props.height-10,
        width: this.props.width-10
    }
    return (
      <div style={style} className="TaskTask">
      <p style={{float:"left"}}> <FaInfoCircle/> information </p>
      <br/>
      <br/>

      <label style={{float:"left",font:"bold"}}>customer Name :</label> 
      <br/>
   <p> {this.props.customerName} </p>
   <br/>
   <label  style={{float:"left"}}> Zone Number </label> 
   <br/>
   <p>    {this.props.zone} </p>
      </div>
    )
  }
}
