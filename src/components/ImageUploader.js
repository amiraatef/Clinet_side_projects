import React from 'react';
import ImageUploader from 'react-images-upload';
import './Styles.css'
import { connect } from "react-redux";
import {Meteor} from 'meteor/meteor'
export   class Image extends React.Component {
 
    constructor(props) {
        super(props);
         this.state = { pictures: [] };
         this.onDrop = this.onDrop.bind(this);

    }
 
    onDrop(picture) {
        // console.log(picture[0])
        this.props.addimage(picture[0],this.props.componentNumber)
        this.setState({
            pictures: this.state.pictures.concat(picture),
        });
    }
 
    render() {
      

     console.log(this.state.pictures)
        return (
            <ImageUploader
            withIcon={false}
            withLabel={false}
            // buttonStyles={}
            withPreview
                buttonText=' upload images'
                onChange={this.onDrop}
                imgExtension={['.jpg', '.gif', '.png', '.gif']}
                maxFileSize={5242880}
            />
        );
    }
}
const mapStateToProps = (state) => {
    return {
  
      ...state.nuggets
  
    }
  }
  const mapDispatchersToProps = (dispatcher) => {
    return {
      addimage: (imagefile) => dispatcher({ type: 'addImageFile', value: imagefile  }),
      setComponentNumber:(Num)=>dispatcher({type:'updateCountNum',value:Num})
    }
  }
  
connect(mapStateToProps, mapDispatchersToProps)(Image);