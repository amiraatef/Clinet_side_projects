import React, { Component } from 'react'
import {Task} from './Task';
import './Style.css';

export  class Tasks extends Component {

  render() {
      let TasksData = this.props.TasksData;
      let Tasks = TasksData.map(TaskData =>{
          return(
                    <Task key={TaskData.customerName}
                          left={TaskData.left}
                          top={TaskData.top}
                          width={TaskData.width}
                          height={TaskData.height}
                          customerName={TaskData.customerName}
                          zone={TaskData.zone}/>
          );
      });

    return (
      <div>
        {Tasks}
      </div>
    )
  }
}
