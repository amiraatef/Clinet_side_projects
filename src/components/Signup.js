import React from 'react';
import {NavLink} from 'react-router-dom'
import {Accounts} from 'meteor/accounts-base'
export  class Signup extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      error: ''
    };
  }
  onSubmit(e)
  {
    e.preventDefault();
    let email = this.refs.email.value.trim();
    let password = this.refs.password.value.trim();
   
    if (password.length < 3) {
      return this.setState({error: 'Password must be more than 8 characters long'});
    }
    
    Accounts.createUser({email, password,dates:[{}],isAdmin:false},(err) => {
      if (err) {
        this.setState({error: err.reason});
      } else {
        this.setState({error: ''});
   
          this.props.history.replace('/Reserve') 
        

      }
    });
      
  
  }
  render() {
  return (
    <div>
           {this.state.error ? <p>{this.state.error}</p> : undefined}
  <form onSubmit={this.onSubmit.bind(this)}>
          <input type="email"  ref="email"name="email" placeholder="Email"/>
          <input type="password" ref="password" name="password" placeholder="Password"/>
          <button>Create Account</button>
        </form>
  <NavLink to="/"> Already have an account </NavLink>
</div>);
  }
}
