const initialState = {

    week:true,
    dayOfMonth:true,
    monthOfYear:true,
    DayandMonth:true
    
    }
    
    const reducer = (state=initialState,action) => {
    
    if (action.type === "SET_week")
    
    {
        return{
            ...state,
            week:!action.value
    
        }
    
    }
    else if (action.type ==="SET_DayofMonth")
    {
        return{
            ...state,
            dayOfMonth:!action.value
    
        }
    }
    else if (action.type==="SET_MonthOfYear")
    {
        return{
            ...state,
            monthOfYear:!action.value
    
        }
    }
    else if (action.type==="SETDAYANDMONTH")
    {
        return{
            ...state,
            DayandMonth:!action.value
    
        }
    }
    return state;
    }
    export default reducer;
    