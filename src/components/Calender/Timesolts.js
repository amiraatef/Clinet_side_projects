import React, { Component } from 'react'
import moment from 'moment';
import '../Calender/Style.css';

export class TimeSchedule extends Component {
    intervals = (startString, endString) => {
        var start = moment.utc(startString, ' hh:mm a');
        var end = moment(endString, 'hh:mm a');
        start.minutes(Math.ceil(start.minutes() / 15) * 15);

        var result = [];

        var current = moment(start);

        while (current <= end) {
            result.push(current.format('hh:mm a'));
            current.add(15, 'minutes');
        }
        return result
    }
 

    render() {
        const times = this.intervals('9:00AM', '6:00PM')
        return (

            <div>
                <table  id="Table" class="TimeSlotestg">

                    <tr>
                        <th class="TimeSlotesfirst">time</th>
                        <th class="TimeSlotestg-us36">User 1 or Zone 1</th>
                        <th class="TimeSlotestg-us36">User 2 or Zone 2</th>
                        <th class="TimeSlotestg-us36">User 3 or Zone 3</th>
                        <th class="TimeSlotestg-us36">User 4 or Zone 4</th>
                    </tr>

                    {times.map((T, index) => {

                        return (
                            <tr>
                                <td style={{padding:'10px'}} class="TimeSlotesfirst" key={index}>{T}</td>
                                <td class="TimeSlotestg-us36"></td>
                                <td class="TimeSlotestg-us36"></td>
                                <td class="TimeSlotestg-us36"></td>
                                <td class="TimeSlotestg-us36"></td>
                            </tr>
                        )


                    })}

                </table>
            </div>

        )
    }
}




