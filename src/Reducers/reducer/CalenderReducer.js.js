import moment from 'moment'
const initialState = {

    open: false,
    From: '',
    To: '',
    zoneNumber: "",
    CustomerName: "",
    top: 0,
    left: 0,
    height: 0,
    right: 0,
    bottom: 0,
    width: 0,
    TasksData:[],
    FormIsNotValid: false,
    timeslots: ["09:00 am", "09:15 am", "09:30 am", "09:45 am", "10:00 am", "10:15 am", "10:30 am", "10:45 am", "11:00 am", "11:15 am", "11:30 am", "11:45 am", "12:00 pm", "12:15 pm", "12:30 pm", "12:45 pm", "01:00 pm", "01:15 pm", "01:30 pm", "01:45 pm", "02:00 pm", "02:15 pm", "02:30 pm", "02:45 pm", "03:00 pm", "03:15 pm", "03:30 pm", "03:45 pm", "04:00 pm", "04:15 pm", "04:30 pm", "04:45 pm", "05:00 pm", "05:15 pm", "05:30 pm", "05:45 pm", "06:00 pm"],
    msg:"Your time slots are taken before ... !",
    OldDate: moment().format()

}

const reducer = (state = initialState, action) => {
    if (action.type === 'SET_TASKSDATA') {
        return {
            ...state,
            TasksData:  action.value
        }
    }
    else if (action.type === 'SET_FORM_VALIDITY') {
        return {
            ...state,
            FormIsNotValid: action.value

        }
    }
    else if (action.type === 'SET_OPEN_FLAG') {
        return {
            ...state,
            open: action.value

        }
    }
    else if (action.type === 'SET_FORM_FIELD') {
        return {

            ...state,
            From: action.value
        }
    }
    else if (action.type === 'SET_TO_FIELD') {
        return {
            ...state,
            To: action.value
        }
    }
    else if (action.type === 'SET_ZONENUMBER_FIELD') {
        return {
            ...state,
            zoneNumber: action.value
        }

    }
    else if (action.type === 'SET_CUSTOMER_NAME_FIELD') {
        return {
            ...state,
            CustomerName: action.value
        }

    }
    else if (action.type ==='SET_message')
    return{
        ...state,
        msg:action.value
          }
          else if(action.type === 'SetDate_')
          {
          return{
              ...state,
              OldDate:action.value
          }
        }
        return state;
}

export default reducer;