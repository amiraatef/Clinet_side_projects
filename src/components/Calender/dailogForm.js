import React,{Component} from 'react';
import { Field, reduxForm } from 'redux-form';
import { TimePicker, TextField } from 'form-material-ui';
import moment from 'moment';
import FlatButton from 'material-ui/FlatButton';


export class DialogForm extends Component {
  required = value => (value ? undefined : 'Required')
     number = value =>{
    1<=value && value>=4 ? 'Must be a number between 1-4' : undefined}
     renderField = ({
        input,
        label,
        type,
        meta: { touched, error, warning }
      }) => (
        <div>
          <label>{label}</label>
          <div>
            <input {...input} placeholder={label} type={type} />
            {touched &&
              ((error && <span style={{color:'red'}}>{error}</span>) ||
                (warning && <span style={{color:'red'}}>{warning}</span>))}
          </div>
        </div>
      )
       time =(value,values) =>
      {
          if(!value)
          {
        
              return
          }
          var from = moment(values.FromTime ,'hh:mm a')
          var to = moment(values.ToTime ,'hh:mm a')

          if(to.isBefore(from))
          {
              return 'Please check your times carefully'
          }

  if(values.ToTime&& values.FromTime)     
     {

      let x= moment(value, ' hh:mm a');
      
          if(x.isBefore( moment('09:00 am', 'hh:mm a')) || x.isAfter( moment('06:00 pm', 'hh:mm a'))){
              value=undefined;
      return 'not valid'
          }
          else{
              var start = moment(values.FromTime);
              var end = moment(values.ToTime);
              alert("kokokok")
              
              Meteor.call('CheckAvailablity',{
                ...values,
                currentDate: this.props.currentDate
            })
// //
         for (let key in this.props.times) {
        var CusttomomentFrom =  moment (this.props.times[key].OldFrom)
        var CusttomomentTo = moment(this.props.times[key].oldTo)
        console.log("Custom From",CusttomomentFrom)
                if (values.ZoneNumber === this.props.times[key].zone)
                {
                    if (  CusttomomentFrom.isBetween(start, end) ||CusttomomentTo.isBetween(start, end)) {
             
                       return 'Time is intersected with Reservation before  please choose another zone , time , or day ';
    ;
    
                     }
           
 
                  
              }
                    }
                
              
          }
      
  }
  
  
  }    
  render() {
    
  




  
return (
    <form onSubmit={this.props.handleSubmit}>
        <Field name="CustomerName" component={TextField} hintText=" Customer Name "  validate={[this.required]} />
        <br/>
        <Field name="ZoneNumber" component={TextField}  hintText="Zone Name"  validate={[this.required, this.number]}/>
        <Field name="FromTime" component={TimePicker}       autoOk={true}  validate={this.time}/>
        <Field name="ToTime" component={TimePicker}     autoOk={true}  validate={this.time} />
        <FlatButton primary={true} style={{float:"right"}} type="submit" label="Submit" onClick = {this.props.clicekd}/>
        <FlatButton primary={true} style={{float:"right"}} label="Cancel" onClick = {this.props.cancelled}/>

    </form>
);
    
  }
}

reduxForm({
    form: 'Dailog'
})(DialogForm)






