import React, { Component } from 'react'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import {Provider} from 'react-redux';
import {Routes} from './routes'
export  class AppRoutes extends Component {
  render() {
    return (
          <MuiThemeProvider>
        <Provider>
<div>
     <Routes/> 
</div>

</Provider>
</MuiThemeProvider>

    )
  }
}
