import "meteor-client";
import React from "react";

import { Route, Switch, BrowserRouter as Router } from "react-router-dom";
import { createStore } from "redux";

import createHistory from "history/createBrowserHistory";

import { Meteor } from "meteor/meteor";
import {Nugget} from "../components/Nuggetscom";

import { AdminPage } from "../components/Admin";
import {App} from "../components/Calender/CalenderApp";
import {Login} from "../components/Login";
import { Signup } from "../components/Signup";
import rootreducer from "../Reducers/reducer/index";
const store = createStore(rootreducer);
const history = createHistory();

const unauthenticatedPages = ["/", "/signup"];
const authenticatedPages = ["/admin", "/Reserve"];

const onEnterPrivatePage = () => {
  {
    if (Meteor.loggingIn()) {
      Meteor._debug();
      debugger;
      console.log(Meteor.user());

      if (Meteor.user().isAdmin == true) {
        Meteor.subscribe("myuser");
        console.log(Meteor.user());

        history.replace("/admin");
      } else {
        Meteor.subscribe("myuser");
        console.log(Meteor.user());
        history.replace("/Reserve");
      }
    }
  }
};

export const onAuthChange = isAuthenticated => {
  const pathname = history.location.pathname;
  const isAuthenticatedPage = authenticatedPages.includes(pathname);
  if (isAuthenticatedPage && !isAuthenticated) {
    history.replace("/");
  }
};

export const Routes = (
  <div>
    <Router history={history}>
      <Switch>
        <div>
          <Route
            exact
            path="/"
            component={Login}
            onEnter={onEnterPrivatePage}
          />
          <Route
            path="/signup"
            component={Signup}
            onEnter={onEnterPrivatePage}
          />
          <Route
            path="/admin"
            component={AdminPage}
            onEnter={onEnterPrivatePage}
          />
          <Route path="/Reserve" component={App} onEnter={onEnterPrivatePage} />
          <Route path="/Nugget" component={Nugget} />
        </div>
        {/* <Route  exact path="*" component={NotFound}/> */}
      </Switch>
    </Router>
  </div>
);
