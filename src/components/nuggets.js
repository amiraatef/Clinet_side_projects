import React, { Component } from 'react'
import './Styles.css'
import { connect } from "react-redux";
import 'meteor-client'
import {Meteor} from 'meteor/meteor'
import {Image }from './ImageUploader'
var interact = require('interact.js')
export class Nuggetsub extends Component {
state={
Imeges:[],
texts:[]
}

    render() {
        // console.log("welcomeToMeteor",Meteor)
        const AddText=()=>{
            var newText= <textarea></textarea>
            this.setState(prevState => ({
                texts: [...prevState.texts, newText]
              }))       


        }
        const AddImage=()=>{
            console.log(this.props)
            var count= this.props.componentNumber
            console.log("befor",count)
            this.props.setComponentNumber(count+1)
     
           console.log("after",this.props.componentNumber)
            var newcompoent=  <Image/>              

            this.setState(prevState => ({
                Imeges: [...prevState.Imeges, newcompoent]
              }))   
                 }
        function dragMoveListener(event) {
            var target = event.target,
                // keep the dragged position in the data-x/data-y attributes
                x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
                y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

            // translate the element
            target.style.webkitTransform =
                target.style.transform =
                'translate(' + x + 'px, ' + y + 'px)';

            // update the posiion attributes
            target.setAttribute('data-x', x);
            target.setAttribute('data-y', y);
        }

        interact('.resize-drag')
            .draggable({
                // enable inertial throwing
                inertia: true,
                // keep the element within the area of it's parent
                restrict: {
                    restriction: "parent",
                    endOnly: true,
                    elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
                },
                // enable autoScroll
                autoScroll: true,

                // call this function on every dragmove event
                onmove: dragMoveListener,
                // call this function on every dragend event
                onend: function (event) {
                    var textEl = event.target.querySelector('p');

                    textEl && (textEl.textContent =
                        'moved a distance of '
                        + (Math.sqrt(Math.pow(event.pageX - event.x0, 2) +
                            Math.pow(event.pageY - event.y0, 2) | 0))
                            .toFixed(2) + 'px');
                },
            })
            .resizable({
                // resize from all edges and corners
                edges: { left: true, right: true, bottom: true, top: true },

                // keep the edges inside the parent
                restrictEdges: {
                    outer: 'parent',
                    endOnly: true,
                },

                // minimum size
                restrictSize: {
                    min: { width: 100, height: 50 },
                },

                inertia: true,
            })
            .on('resizemove', function (event) {
                var target = event.target,
                    x = (parseFloat(target.getAttribute('data-x')) || 0),
                    y = (parseFloat(target.getAttribute('data-y')) || 0);

                // update the element's style
                target.style.width = event.rect.width + 'px';
                target.style.height = event.rect.height + 'px';

                // translate when resizing from top or left edges
                x += event.deltaRect.left;
                y += event.deltaRect.top;

                target.style.webkitTransform = target.style.transform =
                    'translate(' + x + 'px,' + y + 'px)';

                target.setAttribute('data-x', x);
                target.setAttribute('data-y', y);
                //  target.textContent = Math.round(event.rect.width) + '\u00D7' + Math.round(event.rect.height);
            }

            );
      
        return (

            <div  id="container"className="resize-container">
            <a target="_blank">Welcome</a>
            <div>
            <button className="N_Button" onClick={AddImage}>+Image</button>
            <br/>
            <br/>

            <button className="N_Button"  onClick={AddText}>+Text</button>
            <br/>
            <br/>

            </div>
                {/* <div className="resize-drag">
                    <ImageUploader  />
                </div> */}
                 {this.state.Imeges.map((img, index) => (
                                        <div className="resize-drag" key={index}>   {img}   </div>

    ))}

                 {this.state.texts.map((text, index) => (
                                        <div className="resize-drag" key={index}>   {text}   </div>

    ))
    
    
    }
     
            </div>
            
        )
    }
}
const mapStateToProps = (state) => {
    return {
  
      ...state
  
    }
  }
  const mapDispatchersToProps = (dispatcher) => {
    return {
        addTextValue: (Textvalue) => dispatcher({ type: 'addTextFile', value: Textvalue }),
        setComponentNumber:(Num)=>dispatcher({type:'updateCountNum',value:Num})

    }
  }
  
   connect(mapStateToProps, mapDispatchersToProps)(Nuggetsub);